package com.gautam.recipeapplication.viewmodel;

import android.content.Context;
import android.view.View;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.gautam.recipeapplication.util.Util;
import com.gautam.recipeapplication.webservice.Hit;
import com.gautam.recipeapplication.webservice.RecipeResponse;
import com.gautam.recipeapplication.helper.APIClient;
import com.gautam.recipeapplication.helper.APIInterface;
import com.google.android.material.snackbar.Snackbar;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeViewModel extends ViewModel {

    private final MutableLiveData<List<Hit>> listMutableLiveData;
    private MutableLiveData<Boolean> network;
    APIInterface apiInterface;

    public HomeViewModel() {
        listMutableLiveData = new MutableLiveData<>();
        listMutableLiveData.setValue(new ArrayList<>());
        network = new MutableLiveData<>();
        network.setValue(true);
        apiInterface = APIClient.getClient().create(APIInterface.class);

    }

    public LiveData<List<Hit>> getList() {
        return listMutableLiveData;
    }

    public LiveData<Boolean> getNetworkState() {
        return network;
    }

    public void initSearch(String search) {
        Call<RecipeResponse> recipeCall = apiInterface.doGetUserList(search);
        recipeCall.enqueue(new Callback<RecipeResponse>() {
            @Override
            public void onResponse(@NotNull Call<RecipeResponse> call, @NotNull Response<RecipeResponse> response) {
                RecipeResponse recipeResponse = response.body();
                List<Hit> recipeList = recipeResponse.getHits();
                listMutableLiveData.postValue(recipeList);
            }

            @Override
            public void onFailure(@NotNull Call<RecipeResponse> call, @NotNull Throwable t) {
                listMutableLiveData.postValue(new ArrayList<>());
            }
        });

    }

    public void checkNetworkState(Context context) {
        if (!Util.isConnectingToInternet(context)) {
            network.postValue(false);
        }
    }
}