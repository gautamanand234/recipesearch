package com.gautam.recipeapplication.viewmodel;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.gautam.recipeapplication.model.DBHelper;
import com.gautam.recipeapplication.model.RecipeDetail;

import java.util.ArrayList;
import java.util.List;

public class FavoriteViewModel extends ViewModel {
    private MutableLiveData<List<RecipeDetail>> listMutableLiveData;
    DBHelper apiInterface;

    public FavoriteViewModel() {
        listMutableLiveData = new MutableLiveData<>();
        listMutableLiveData.setValue(new ArrayList<>());


    }
    public void intializeDB(Context context){
        apiInterface =new DBHelper(context);
    }

    public LiveData<List<RecipeDetail>> getList() {
        return listMutableLiveData;
    }

    public void initSearch() {
        ArrayList<RecipeDetail> recipeCall = apiInterface.getAllRecipe();
        listMutableLiveData.postValue(recipeCall);

    }
}