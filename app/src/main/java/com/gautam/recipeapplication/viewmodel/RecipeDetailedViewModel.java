package com.gautam.recipeapplication.viewmodel;

import android.content.Context;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.gautam.recipeapplication.model.RecipeDetail;
import com.gautam.recipeapplication.model.DBHelper;

import java.util.ArrayList;
import java.util.List;


public class RecipeDetailedViewModel extends ViewModel {
    private MutableLiveData<List<RecipeDetail>> listMutableLiveData;
    DBHelper dbHelper;

    public RecipeDetailedViewModel() {
        listMutableLiveData = new MutableLiveData<>();
        listMutableLiveData.setValue(new ArrayList<>());


    }

    public void intializeDB(Context context) {
        dbHelper = new DBHelper(context);
    }

    public void insertDB(RecipeDetail recipeDetail) {
        dbHelper.insertImage(recipeDetail.getLabel(), recipeDetail.getDesc(), recipeDetail.getBitmap());
    }


}
