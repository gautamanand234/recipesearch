package com.gautam.recipeapplication.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.gautam.recipeapplication.R;
import com.gautam.recipeapplication.webservice.Hit;
import com.gautam.recipeapplication.webservice.Recipe_;
import com.gautam.recipeapplication.model.RecipeDetail;
import com.gautam.recipeapplication.helper.RxBus;
import com.gautam.recipeapplication.util.Util;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.apache.commons.lang3.StringUtils;

import java.util.List;

public class RecipeOnlineAdapter extends RecyclerView.Adapter<RecipeOnlineAdapter.RecipesViewHolder> {
    Context context;
    List<Hit> recipeList;


    public RecipeOnlineAdapter(Context context, List<Hit> recipeList) {
        this.context = context;
        this.recipeList = recipeList;
    }

    @NonNull
    @Override
    public RecipesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_holder, parent, false);
        return new RecipesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecipesViewHolder holder, int position) {
        Recipe_ recipe = recipeList.get(position).getRecipe();
        holder.tvHeader.setText(recipe.getLabel());
        String listString = StringUtils.join(recipe.getIngredientLines(), ", ");
        holder.tvContent.setText(listString);

        RecipeDetail recipeDetail = new RecipeDetail();
        recipeDetail.setDesc(listString);
        recipeDetail.setLabel(recipe.getLabel());

        Picasso.get().load(recipe.getImage()).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap1, Picasso.LoadedFrom from) {
                recipeDetail.setBitmap(Util.BitmaptoByteArray(bitmap1));
                holder.ivRecipe.setImageBitmap(bitmap1);
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        });

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RxBus.getInstance().publish(recipeDetail);
            }
        });

    }

    @Override
    public int getItemCount() {
        return recipeList.size();
    }

    public static class RecipesViewHolder extends RecyclerView.ViewHolder {
        ImageView ivRecipe;
        TextView tvHeader;
        TextView tvContent;
        CardView cardView;

        public RecipesViewHolder(@NonNull View itemView) {
            super(itemView);
            ivRecipe = itemView.findViewById(R.id.iv_recipe);
            tvContent = itemView.findViewById(R.id.tv_description);
            tvHeader = itemView.findViewById(R.id.tv_title);
            cardView = itemView.findViewById(R.id.cv_item);
        }
    }
}
