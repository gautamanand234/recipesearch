package com.gautam.recipeapplication.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.gautam.recipeapplication.R;
import com.gautam.recipeapplication.model.RecipeDetail;
import com.gautam.recipeapplication.ui.main.RecipeDetailedActivity;
import com.gautam.recipeapplication.util.Util;

import java.util.List;

public class RecipeOfflineAdapter extends RecyclerView.Adapter<RecipeOfflineAdapter.RecipesViewHolder> {
    private final Context context;
    private final List<RecipeDetail> recipeList;

    public RecipeOfflineAdapter(Context context, List<RecipeDetail> recipeList) {
        this.context = context;
        this.recipeList = recipeList;
    }

    @NonNull
    @Override
    public RecipesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_holder, parent, false);
        return new RecipesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecipesViewHolder holder, int position) {
        RecipeDetail recipe = recipeList.get(position);
        holder.tvHeader.setText(recipe.getLabel());
        holder.tvContent.setText(recipe.getDesc());
        holder.ivRecipe.setImageBitmap(Util.getBitmap(recipe.getBitmap()));


        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(context, RecipeDetailedActivity.class);
                intent.putExtra("Detail", recipe);
                context.startActivity(intent);
                // RxBus.getInstance().publish(detail);
            }
        });
    }


    @Override
    public int getItemCount() {
        return recipeList.size();
    }

    public static class RecipesViewHolder extends RecyclerView.ViewHolder {
        ImageView ivRecipe;
        TextView tvHeader;
        TextView tvContent;
        CardView cardView;

        public RecipesViewHolder(@NonNull View itemView) {
            super(itemView);
            ivRecipe = itemView.findViewById(R.id.iv_recipe);
            tvContent = itemView.findViewById(R.id.tv_description);
            tvHeader = itemView.findViewById(R.id.tv_title);
            cardView = itemView.findViewById(R.id.cv_item);
        }
    }
}
