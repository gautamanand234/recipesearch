package com.gautam.recipeapplication.model;

import android.graphics.Bitmap;

import java.io.Serializable;

public class RecipeDetail implements Serializable {
    byte[] bitmap;
    String label;
    String desc;

    public RecipeDetail() {
        bitmap = new byte[0];
    }

    public RecipeDetail(byte[] bitmap, String label, String desc) {
        this.bitmap = bitmap;
        this.label = label;
        this.desc = desc;
    }

    public byte[] getBitmap() {
        return bitmap;
    }

    public void setBitmap(byte[] bitmap) {
        this.bitmap = bitmap;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
