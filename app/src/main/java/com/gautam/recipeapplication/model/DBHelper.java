package com.gautam.recipeapplication.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class DBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "Recipes.db";
    public static final String RECIPE_TABLE_NAME = "recipeoffline";
    public static final String RECIPE_COLUMN_NAME = "name";

    public DBHelper(Context context)
    {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        db.execSQL(
                "create table recipeoffline " +
                        "(id integer primary key, label text,steps text,image blob)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS recipeoffline");
        onCreate(db);
    }

    public boolean insertImage(String label, String steps, byte[] image) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("label", label);
        contentValues.put("steps", steps);
        contentValues.put("image", image);

        db.insert("recipeoffline", null, contentValues);
        return true;
    }

    public Cursor getData(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from recipeoffline where id=" + id + "", null);
        return res;
    }

    public int numberOfRows() {
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, RECIPE_TABLE_NAME);
        return numRows;
    }


    public Integer deleteContact(Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("recipeoffline",
                "id = ? ",
                new String[]{Integer.toString(id)});
    }

    public ArrayList<RecipeDetail> getAllRecipe() {
        try {
            ArrayList<RecipeDetail> array_list = new ArrayList<>();

            //hp = new HashMap();
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor res = db.rawQuery("select * from recipeoffline", null);
            res.moveToFirst();

            while (!res.isAfterLast()) {
                RecipeDetail recipeDetail = new RecipeDetail();
                recipeDetail.setLabel(res.getString(res.getColumnIndex("label")));
                recipeDetail.setDesc(res.getString(res.getColumnIndex("steps")));
                recipeDetail.setBitmap(res.getBlob(res.getColumnIndex("image")));
                array_list.add(recipeDetail);
                res.moveToNext();
            }
            return array_list;
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }
}
