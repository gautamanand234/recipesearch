package com.gautam.recipeapplication.ui.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gautam.recipeapplication.MainActivity;
import com.gautam.recipeapplication.R;
import com.gautam.recipeapplication.adapter.RecipeOnlineAdapter;
import com.gautam.recipeapplication.helper.RxBus;
import com.gautam.recipeapplication.viewmodel.HomeViewModel;
import com.google.android.material.snackbar.Snackbar;

import io.reactivex.rxjava3.disposables.Disposable;


/**
 * A placeholder fragment containing a simple view.
 */
public class HomeFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";

    private HomeViewModel homeViewModel;

    RecyclerView recyclerView;
    RecipeOnlineAdapter recipeOnlineAdapter;
    Context context;
    RxBus rxBus;
    Disposable disposable;

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_main, container, false);
        homeViewModel = new ViewModelProvider(this).get(HomeViewModel.class);
        setHasOptionsMenu(true);
        context = this.getContext();
        recyclerView = root.findViewById(R.id.rv_all);
        rxBus = RxBus.getInstance();
        homeViewModel.checkNetworkState(context);
        listenObserves();
        homeViewModel.getList().observe(getViewLifecycleOwner(), hits -> {
            recipeOnlineAdapter = new RecipeOnlineAdapter(context, hits);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            recyclerView.setLayoutManager(linearLayoutManager);
            recyclerView.setAdapter(recipeOnlineAdapter);
        });

        homeViewModel.getNetworkState().observe(getViewLifecycleOwner(), aBoolean -> {
            if (!aBoolean)
                Toast.makeText(context, "Internet is not connected", Toast.LENGTH_SHORT).show();
        });

        return root;
    }

    @Override
    public void onStart() {
        super.onStart();
        homeViewModel.checkNetworkState(context);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menusearch, menu);
        MenuItem item = menu.findItem(R.id.action_search);
        SearchView searchView = new SearchView(((MainActivity) context).getSupportActionBar().getThemedContext());
        // MenuItemCompat.setShowAsAction(item, //MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | //MenuItemCompat.SHOW_AS_ACTION_IF_ROOM);
        //  MenuItemCompat.setActionView(item, searchView);
        // These lines are deprecated in API 26 use instead
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | MenuItem.SHOW_AS_ACTION_IF_ROOM);
        item.setActionView(searchView);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                homeViewModel.initSearch(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

    }

    private void listenObserves() {
        disposable = rxBus.listen()
                .subscribe(recipe_ -> {
                    Intent intent = new Intent(getActivity(), RecipeDetailedActivity.class);
                    intent.putExtra("Detail", recipe_);
                    context.startActivity(intent);

                });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }
    }
}