package com.gautam.recipeapplication.ui.main;

import android.os.Bundle;

import com.gautam.recipeapplication.model.RecipeDetail;
import com.gautam.recipeapplication.util.Util;
import com.gautam.recipeapplication.viewmodel.RecipeDetailedViewModel;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;

import android.widget.ImageView;
import android.widget.TextView;

import com.gautam.recipeapplication.R;

public class RecipeDetailedActivity extends AppCompatActivity {


    private RecipeDetailedViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        CollapsingToolbarLayout toolBarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        TextView textView = findViewById(R.id.tv_steps);
        ImageView imageView = toolBarLayout.findViewById(R.id.backdrop);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        setSupportActionBar(toolbar);

        RecipeDetail recipeDetail = (RecipeDetail) getIntent().getSerializableExtra("Detail");
        mViewModel = new ViewModelProvider(this).get(RecipeDetailedViewModel.class);
        mViewModel.intializeDB(getApplicationContext());
        if (recipeDetail != null) {
            toolBarLayout.setTitle(recipeDetail.getLabel());
            textView.setText(recipeDetail.getDesc());
            byte[] image = recipeDetail.getBitmap();
            if (image.length > 0)
                imageView.setImageBitmap(Util.getBitmap(image));
        }
        fab.setOnClickListener(view -> mViewModel.insertDB(recipeDetail));


    }


}