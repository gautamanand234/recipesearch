package com.gautam.recipeapplication.ui.main;

import androidx.lifecycle.ViewModelProvider;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gautam.recipeapplication.R;
import com.gautam.recipeapplication.adapter.RecipeOfflineAdapter;
import com.gautam.recipeapplication.helper.RxBus;
import com.gautam.recipeapplication.viewmodel.FavoriteViewModel;

public class FavoriteFragment extends Fragment {

    private FavoriteViewModel mViewModel;

    public static FavoriteFragment newInstance() {
        return new FavoriteFragment();
    }

    RecyclerView recyclerView;
    RecipeOfflineAdapter recipeAdapter;
    Context context;
    RxBus rxBus;


    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_main, container, false);
        context = this.getContext();
        mViewModel = new ViewModelProvider(this).get(FavoriteViewModel.class);
        mViewModel.intializeDB(context.getApplicationContext());
        recyclerView = root.findViewById(R.id.rv_all);
        mViewModel.initSearch();
        setHasOptionsMenu(true);
        mViewModel.getList().observe(getViewLifecycleOwner(), hits -> {
            recipeAdapter = new RecipeOfflineAdapter(context, hits);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            recyclerView.setLayoutManager(linearLayoutManager);
            recyclerView.setAdapter(recipeAdapter);
        });
        return root;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            mViewModel.initSearch();
        }
    }
}