package com.gautam.recipeapplication.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Base64;

import java.io.ByteArrayOutputStream;
import java.util.Calendar;

public class Util {

    public static byte[] BitmaptoByteArray(Bitmap encodedString) {
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            encodedString.compress(Bitmap.CompressFormat.PNG, 0, outputStream);
            return outputStream.toByteArray();

        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }

    public static Bitmap getBitmap(byte[] encodedImage) {
        return BitmapFactory.decodeByteArray(encodedImage, 0, encodedImage.length);
    }
    public static boolean isConnectingToInternet(Context context){
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null)
        {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            for (NetworkInfo networkInfo : info)
                if (networkInfo.getState() == NetworkInfo.State.CONNECTED) {
                    return true;
                }

        }
        return false;
    }

}
