package com.gautam.recipeapplication.helper;



import com.gautam.recipeapplication.webservice.RecipeResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface APIInterface {//q=samosa
    @GET("/search?app_id=1b5a48ee&app_key=582e8c858ac93318cf5289dc460de26d&")
    Call<RecipeResponse> doGetUserList(@Query("q") String recipe);

  /*  @GET
    Call<String> getImage(@Url String url);*/

}
