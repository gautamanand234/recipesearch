package com.gautam.recipeapplication.helper;


import com.gautam.recipeapplication.model.RecipeDetail;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.subjects.PublishSubject;

public class RxBus {
    private static RxBus RX_BUS;
    private final PublishSubject<RecipeDetail> publisher;

    private RxBus() {
        publisher = PublishSubject.create();
    }

    public static RxBus getInstance() {
        if (RX_BUS == null)
            RX_BUS = new RxBus();
        return RX_BUS;
    }

    public void publish(RecipeDetail event) {
        publisher.onNext(event);
    }

    // Listen should return an Observable and not the publisher
    // Using ofType we filter only events that match that class type
    public Observable<RecipeDetail> listen() {
        return publisher;
    }
}

